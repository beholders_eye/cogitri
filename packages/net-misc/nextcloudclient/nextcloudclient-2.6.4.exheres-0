# Copyright 2017 Rasmus Thomsen
# Distributed under the terms of the GNU General Public License v2

require cmake [ api=2 ]
require gtk-icon-cache
require github [ user=nextcloud project=desktop tag=v${PV} ]

SUMMARY="Nextcloud themed desktop client"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~x86"

MYOPTIONS="
    dolphin [[ description = [ Build dolphin extensions ] ]]
    shibboleth [[ description = [ Support Shibboleth authentication,
        app passwords are a viable alternative ] ]]
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-db/sqlite:3[>=3.8.0]
        sys-auth/qtkeychain[qt5(+)]
        sys-libs/zlib
        x11-libs/qtbase:5[gui][sql][sqlite]
        x11-libs/qttools:5
        x11-libs/qtwebengine:5
        dolphin? ( kde-frameworks/kio:5 )
        shibboleth? ( x11-libs/qtwebkit:5 )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl[>=1.1] )
    test:
        dev-util/cmocka
    suggestion:
        dev-python/nautilus-python [[ description = [ To enable sync icons in
            nautilus indicating which files have been synched and which haven't been ] ]]
"

CMAKE_SOURCE="${WORKBASE}"/desktop-${PV}

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}/${PNV}-libressl.patch"
    "${FILES}"/${PN}-Fix-include-file.patch
    "${FILES}"/${PN}-Fix-build-with-Qt-5.15.patch
)

CMAKE_SRC_CONFIGURE_OPTIONS=(
    'dolphin BUILD_SHELL_INTEGRATION_DOLPHIN'
    '!shibboleth NO_SHIBBOLETH'
)

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_WITH_QT4:BOOL=FALSE
    -DDATA_INSTALL_DIR:PATH=/usr/share
    -DSHARE_INSTALL_PREFIX:PATH=/usr/share
    -DCMAKE_INSTALL_DOCDIR:PATH=/usr/share/doc/${PNVR}
    -DSYSCONF_INSTALL_DIR:PATH=/etc
)

CMAKE_SRC_CONFIGURE_TESTS=( '-DUNIT_TESTING:BOOL=TRUE -DUNIT_TESTING:BOOL=FALSE' )

src_test() {
    export HOME="${TEMP}"
    # unset DISPLAY, so that it doesn't try to reach our X server
    unset DISPLAY
    default
}

