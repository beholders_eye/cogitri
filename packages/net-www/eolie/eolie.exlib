# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

# Eolie requires a submodule ( not packaged in tarballs ) and
# gitlab.exlib doesn't support force cloning releases yet
SCM_REPOSITORY="https://gitlab.gnome.org/World/eolie.git"
SCM_po_REPOSITORY="https://gitlab.gnome.org/World/eolie-po.git"
SCM_SECONDARY_REPOSITORIES="po"
SCM_EXTERNAL_REFS="subprojects/po:po"

ever is_scm || SCM_TAG="${PV}"

require scm-git
require python [ blacklist=2 multibuild=false ]
require gtk-icon-cache gsettings
require meson

export_exlib_phases src_prepare pkg_postinst pkg_postrm

SUMMARY="Web browser for GNOME"

LICENCES="GPL-3"
SLOT="0"

MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        app-spell/gtkspell:3.0
        dev-libs/glib:2
        dev-python/pycairo[python_abis:*(-)?]
        dev-python/python-dateutil[python_abis:*(-)?]
        gnome-bindings/pygobject:3[python_abis:*(-)?]
        gnome-desktop/gobject-introspection:1[>=1.35.9][python_abis:*(-)?]
        net-libs/webkit:4.0[>=2.16][gobject-introspection]
        x11-libs/gtk+:3[>=3.22]
    recommendation:
        (
            dev-python/beautifulsoup4[python_abis:*(-)?]
            dev-python/cryptography[python_abis:*(-)?]
            dev-python/pycrypto[python_abis:*(-)?]
            dev-python/PyFxA[python_abis:*(-)?]
            dev-python/requests-hawk[python_abis:*(-)?]
        ) [[
            *description = [ Required for Firefox Sync ]
        ]]
"

# Appdata validation needs to download files for testing
RESTRICT="test"

eolie_src_prepare() {
    meson_src_prepare

    # Adjust python dir to our file architecture
    sed -e "s:('prefix'), python.sys:('prefix'), '$(exhost --target)', python.sys:" \
        -i meson.build
}

eolie_pkg_postinst() {
    gsettings_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

eolie_pkg_postrm() {
    gsettings_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

