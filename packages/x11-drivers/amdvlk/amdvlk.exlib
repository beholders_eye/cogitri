# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SCM_REPOSITORY="git://github.com/GPUOpen-Drivers/AMDVLK.git"
SCM_xgl_REPOSITORY="git://github.com/GPUOpen-Drivers/xgl.git"
SCM_xgl_CHECKOUT_TO="xgl-amdvlk"
SCM_pal_REPOSITORY="git://github.com/GPUOpen-Drivers/pal.git"
SCM_pal_CHECKOUT_TO="pal-amdvlk"
SCM_wsa_REPOSITORY="git://github.com/GPUOpen-Drivers/wsa.git"
SCM_wsa_CHECKOUT="wsa-amdvlk"
SCM_llvm_REPOSITORY="git://github.com/GPUOpen-Drivers/llvm.git"
SCM_llvm_CHECKOUT_TO="llvm-amdvlk"
SCM_llvm_BRANCH="amd-vulkan-master"
SCM_llpc_REPOSITORY="git://github.com/GPUOpen-Drivers/llpc.git"
SCM_llpc_CHECKOUT_TO="llpc-amdvlk"
SCM_llpc_BRANCH="master"
SCM_SECONDARY_REPOSITORIES="xgl pal llvm wsa llpc"
require scm-git
require cmake [ api=2 ]

export_exlib_phases src_install

SUMMARY="AMD Open Source Driver For Vulkan"

LICENCES="MIT"
SLOT="0"

MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-lang/perl:*[>=5]
        dev-lang/python:*[>=3]
        virtual/pkg-config
        x11-proto/xorgproto
    build+run:
        dev-libs/libxml2:2.0
        x11-libs/libX11
        x11-libs/libxshmfence
        sys-libs/wayland
"

CMAKE_SOURCE="${WORKBASE}/xgl"
WORK="${CMAKE_SOURCE}"

CMAKE_SRC_CONFIGURE_PARAMS=(
    # Doesn't like BUILD_TYPE=None
    -DCMAKE_BUILD_TYPE:STRING=Release
    -DBUILD_WSA:BOOL=TRUE
    -DBUILD_WAYLAND_SUPPORT:BOOL=TRUE
)

amdvlk_src_install() {
    # cmake_src_install would also install AMD's custom llvm, which we don't want.
    dolib wsa/wayland/libamdgpu_wsa_wayland.so
    dolib icd/amdvlk64.so

    insinto /usr/share/vulkan.icd.d
    doins "${WORKBASE}"/${PNV}/json/Redhat/amd_icd64.json
}

